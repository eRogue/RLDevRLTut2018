#20180814
* added os.path.join to data_loaders.py
* add bats
* added random movement for bats
* deleted duplicate \fighter.py


#20180807
* Started JSON -- tutorial extra is not complete.
* Added wall ('#') and floor ('.') "terrain" characters.
	* Implemented render_tile() to draw terrain when clearing entities.

#20180731
* Completed Week 6
* Completed week 7 and the complete roguelike tutorial.

# 20180724
* Fixed fov_recompute bug. indentation error
* Fixed only showing tail of multi-line messages. indentation error
* Fixed Confusion Scroll crashes.
	* err: `targeting_mesage = Message(`
	* fix: `targeting_message = Message(`
* Changed spaces to tabs
* added Part 10 code until 'It's time to think about how we're going to save and load our game.'



Have fun.  
-- e.rogue
