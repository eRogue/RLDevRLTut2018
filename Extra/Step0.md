Download Notepad++ (or whatever editor/IDE)

https://notepad-plus-plus.org/repository/7.x/7.5.1/npp.7.5.1.Installer.x64.exe


1. Install python 3.6.5.  Choose 64- or 32-bit.  
	1. Download and run https://www.python.org/ftp/python/3.6.5/python-3.6.5-amd64-webinstall.exe
	1. Checkmark everything (all the optional components, and to add python to your system path.)  
		+ n.b. for some reason, my path was still unable to find python.exe until I added it for all users by hand.  The py loader worked regardless.
	1. Complete the installation.  

1. Download libtcod 1.7.  Choose same as above 64- or 32-bit.  
	1. Download https://bitbucket.org/libtcod/libtcod/downloads/libtcod-1.7.0-x86_64-msvc.zip  
	1. Extract the zip to it's own directory  

1. Install libtcod through python  
	1. start / run / cmd  
	1. change directory to libtcod's python subdirectory  
		+ `cd ... \libtcod-1.7.0-x86_64-msvc\python`  
	1. `py setup.py install`  

1. Just for kicks, from windows explorer, double click `\libtcod-1.7.0-x86_64-msvc\python\samples_py.py`  
	+ This shows us all the colorful characters we'll be able to use in our Roguelike project -- pretty much what we are using libtcod for.  
	+ Use the arrow keys and onscreen instructions to navigate through demos.  

1. Prepare project directory and main file  
	1. launch editplus  
	1. File / New  
	1. File / Save As / (browse and make new directory) / `engine.py`  

1. Copy libtcod files to your project  
	1. Three dll files  
		+ `\libtcod-1.7.0-x86_64-msvc\libtcod.dll` >> `\RLTut\libtcod.dll`  
		+ `\libtcod-1.7.0-x86_64-msvc\libtcodgui.dll` >> `\RLTut\libtcodgui.dll`  
		+ `\libtcod-1.7.0-x86_64-msvc\SDL2.dll` >> `\RLTut\SDL2.dll` 
	1. The full python\libtcodpy\ directory  
		+ `\libtcod-1.7.0-x86_64-msvc\python\libtcodpy\.` >> `\RLTut\libtcodpy\.`  
	1. A font file  
		+ `\libtcod-1.7.0-x86_64-msvc\data\fonts\arial10x10.png` >> `\RLTut\arial10x10.png`  
	1. Verify project root directory contents  
![Project root directory content](http://www.roguebasin.com/images/a/a9/Windows-05-162-project-folder-ready.png)  

1. Verify installion of libtcodpy within python  
	+ From the command prompt, see if python is able to import libtcodpy  
		1. start / run / cmd  
		1. `cd RLTut`  
		1. `py -c "import libtcodpy"`  
		1. Should give no errors; when all is good, nothing will be output.  

1. Procede to Part 1!  
	+ http://rogueliketutorials.com/libtcod/1  



Have fun.
-- e.rogue