# Known Bugs
* load_error_mesage does not clear previous message box, which exists in the actual tutorial source.
* No HP text shown at 1 HP
* engine.py: FileNotFoundError: Previous Menu Keys shown buy not functional
	while error messagebox displayed

# Project Overview
* Peruse Tutorial's extras.
	* Change to JSON for saving
* Add screenshot command.
* Name character.
* Name / delete savefiles.
* Randomize damage.
* Add sound effects.
* Add keyboard targeting.
* Add goal, a la "Amulet of Yendor'
* Dungeon generation
	* Add mine.
	* Add Rogue's.
* Help menu

Have fun.  
-- e.rogue