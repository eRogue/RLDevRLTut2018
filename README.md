# RLDevRLTut2018
RoguelikeDev Does The Complete Roguelike Tutorial 2018

# Description  
A roguelike game using python 3.6.5 and libtcod 1.7, developed at https://twitch.tv/erogue, with community support through [reddit](https://www.reddit.com/r/roguelikedev/comments/8s5x5n/roguelikedev_does_the_complete_roguelike_tutorial/).  The videos are archived at [YouTube](https://www.youtube.com/playlist?list=PLSX2U_ZE4HulZFbLJI05mY34_tDelLqp5).

# Table of Contents  
1. [Installation](#installation)
1. [Usage](#usage)
1. [Contributing](#contributing)
1. [Credits](#credits)
1. [License](#license)
1. [Images](#images)

# Installation
* Ensure the following **dependancies** are installed:
	* [Python 3.6](https://www.python.org/downloads/release/python-365/)
	* [libtcod 1.7](https://bitbucket.org/libtcod/libtcod/downloads/)
* Download the project and extract if zipped.

# Usage
Running `engine.py` will launch the game.

# Contributing  
This is a personal project, and I don't expect any outside contributions.  Nonetheless, help is always appreciated.

# Credits
Produced by e.Rogue with support from [the RoguelikeDev community](https://www.reddit.com/r/roguelikedev/).

# License  
Give credit where credit is due.

# Images
![RoguelikeDev Does the Roguelike Tutorial](https://i.imgur.com/EYJFgdI.png)

![RoguelikeDev Does the Roguelike Tutorial](https://gitlab.com/eRogue/RLDevRLTut2018/raw/master/Extra/screenshot.png)
